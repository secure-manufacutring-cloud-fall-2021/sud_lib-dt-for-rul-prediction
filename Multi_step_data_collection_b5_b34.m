% Cycle_Data = [B0053.cycle, B0054.cycle, B0055.cycle, B0056.cycle];
% Cycle_Data = [B0005.cycle, B0006.cycle, B0007.cycle, B0018.cycle, B0025.cycle, B0026.cycle, B0027.cycle, B0028.cycle];
Cycle_Data = [B0005.cycle, B0006.cycle, B0007.cycle, B0018.cycle, B0034.cycle B0036.cycle];
d_cycle = 1;
Cycle_times = [];
for idx = 1:length(Cycle_Data)
    if strcmp(Cycle_Data(idx).type, 'discharge')
        Time_Series_discharge  = Cycle_Data(idx).data.Time;
        Temperature = Cycle_Data(idx).data.Temperature_measured;
        Load_voltage_discharge = Cycle_Data(idx).data.Voltage_load;         %Voltage at load resistance
        Terminal_voltage_discharge = Cycle_Data(idx).data.Voltage_measured; %Battery Terminal voltage
%         l = 1;
%         for k = 2:length(Time_Series_discharge)
%             T_diff(l) = Time_Series_discharge(k) - Time_Series_discharge(k-1);
%             V_diff(l) = Load_voltage_discharge(k) - Load_voltage_discharge(k-1);
%             V_Gradient(l) = V_diff(l) / T_diff(l);
%             l = l + 1;
%         end
%        mean_VGrad(d_cycle) = mean(V_Gradient(abs(V_Gradient) <= 0.001));
        % Extract the TIEDVD
        %V_in_range_idx = find(Load_voltage_discharge>= 1.9 & Load_voltage_discharge <= 2.86);
%         V_in_range_idx = find(Load_voltage_discharge>= 2.33 & Load_voltage_discharge <= 2.92);
%         TIEDVD(d_cycle) = Time_Series_discharge(V_in_range_idx(length(V_in_range_idx))) - Time_Series_discharge(V_in_range_idx(1));
%         mean_VGrad_full(d_cycle) = (Load_voltage_discharge(V_in_range_idx(length(V_in_range_idx))) - Load_voltage_discharge(V_in_range_idx(1))) / TIEDVD(d_cycle) ;
%         %Temp_in_range_idx = find(Temperature >= 28 & Temperature <= 40);
%         Temp_in_range_idx = find(abs(Cycle_Data(idx).data.Current_load) >= 1);        
%         TIETD(d_cycle) = Time_Series_discharge(Temp_in_range_idx(length(Temp_in_range_idx))) - Time_Series_discharge(Temp_in_range_idx(1));
%         Temperature_Gradient_full(d_cycle) = (Temperature(Temp_in_range_idx(length(Temp_in_range_idx))) - Temperature(Temp_in_range_idx(1))) / TIETD(d_cycle) ;
        Capacity_series_full(d_cycle) = Cycle_Data(idx).data.Capacity;
        % Get the rest times and put them in an array
        Cycle_times(:,d_cycle) = Cycle_Data(idx).time;
        d_cycle = d_cycle + 1;
    end
end

% Normalisation
% Capacity_series = [Capacity_series_full(1:55) / Capacity_series_full(2) Capacity_series_full(57:158) / Capacity_series_full(58) Capacity_series_full(160:262) / Capacity_series_full(161) Capacity_series_full(263:362) / Capacity_series_full(263)];
% Capacity_series = [Capacity_series_full(57:159) / Capacity_series_full(58) Capacity_series_full(160:262) / Capacity_series_full(161) Capacity_series_full(263:362) / Capacity_series_full(263)];
Capacity_series = [Capacity_series_full(1:168) / Capacity_series_full(1) Capacity_series_full(169:336) / Capacity_series_full(169) Capacity_series_full(337:504) / Capacity_series_full(337) Capacity_series_full(505:636) / Capacity_series_full(505) Capacity_series_full(637:833) / Capacity_series_full(638) Capacity_series_full(834:1029) / Capacity_series_full(835)];
% Capacity_series = [Capacity_series_full(1:168) Capacity_series_full(169:336) Capacity_series_full(337:504) Capacity_series_full(505:636) Capacity_series_full(637:833) Capacity_series_full(834:1029)];
% T_Cycle_times = transpose(Cycle_times);
T_Cycle_times = transpose(Cycle_times);

%Rest time based approach
Regen_Rest_time = [];
Regen_cycle = [];
for r = 2:length(T_Cycle_times)
    RT = etime(T_Cycle_times(r,:), T_Cycle_times(r-1,:)); 
    if  RT >= 1e+05
        Regen_Rest_time = [Regen_Rest_time RT];
        Regen_cycle(end + 1) = r;
    end
end

Regen_Rest_time = [];
Regen_ampl = [];
SOH = [];
for  k = 1:length(Regen_cycle)
   RT = etime(T_Cycle_times(Regen_cycle(k),:), T_Cycle_times(Regen_cycle(k)-1,:));
   Regen_Rest_time = [Regen_Rest_time RT];
%   C_Diff = abs((IMF_C(Regen_cycle(k)) - IMF_C(Regen_cycle(k)-1)) / IMF_C(Regen_cycle(k)-1));
   C_Diff = abs((Capacity_series(Regen_cycle(k)) - Capacity_series(Regen_cycle(k)-1)) / Capacity_series(Regen_cycle(k)-1));
%   C_Diff = abs((Capacity_series(Regen_cycle(k)) - Capacity_series(Regen_cycle(k)-1)));
   SOH = [SOH Capacity_series(Regen_cycle(k)-1)];
   Regen_ampl = [Regen_ampl C_Diff]; 
end

% Corrections to Regen_ampl
Regen_ampl(24) = 0.0569;
Regen_ampl(28) = 0.0172;
Regen_ampl(31) = 0.0214;
Regen_ampl(32) = 0.0201;

% Regenerated Useful time (RUT)
RUT_B0x = [10 5 7 6 8 3 6 10 10 4 5 6 0 3 4 4 12 3 14 12];
SOH_B0x = SOH(1:20);
Regen_Rest_time_B0x = Regen_Rest_time(1:20);
Regen_ampl_B0x = Regen_ampl(1:20);

% % Exponential fitting, battery B5: a*e^(b*x) + d
% Cap_HP_B5 = highpass(Capacity_series(1:168), 0.04, 'Steepness', 0.95);
% Cap_LP_B5 = lowpass(Capacity_series(1:168), 0.04, 'Steepness', 0.95);
% 
% Cap_HP_B6 = highpass(Capacity_series(169:336), 0.04, 'Steepness', 0.95);
% Cap_LP_B6 = lowpass(Capacity_series(169:336), 0.04, 'Steepness', 0.95);
% 
% Cap_HP_B7 = highpass(Capacity_series(337:504), 0.04, 'Steepness', 0.95);
% Cap_LP_B7 = lowpass(Capacity_series(337:504), 0.04, 'Steepness', 0.95);
% 
% Cap_HP_B18 = highpass(Capacity_series(505:636), 0.04, 'Steepness', 0.95);
% Cap_LP_B18 = lowpass(Capacity_series(505:636), 0.04, 'Steepness', 0.95);

[IMF1, RES1] = emd(Capacity_series(1:168));
[IMF2, RES2] = emd(Capacity_series(169:336));
[IMF3, RES3] = emd(Capacity_series(337:504));
[IMF4, RES4] = emd(Capacity_series(505:636));
[IMF5, RES5] = emd(Capacity_series(638:833));
[IMF6, RES6] = emd(Capacity_series(835:1029));

Cap_HP_B5 = IMF1(:,1) + IMF1(:,2);
Cap_HP_B6 = IMF2(:,1) + IMF2(:,2);
Cap_HP_B7 = IMF3(:,1) + IMF3(:,2);
Cap_HP_B18 = IMF4(:,1) + IMF4(:,2);
Cap_HP_B34 = IMF5(:,1) + IMF5(:,2);
Cap_HP_B36 = IMF6(:,1) + IMF6(:,2);

% Real Residuals
% Cycle_No = 1:length(RES1);
% Real_RES1 = RES1 + IMF1(:,3);
% Real_RES2 = RES2 + IMF2(:,3);
% Real_RES3 = RES3 + IMF3(:,3);
% Cycle_No_RES4 = 1:length(RES4);
% Real_RES4 = RES4 + IMF4(:,3);
% Cycle_No_RES5 = 1:length(RES5);
% Real_RES5 = RES5 + IMF5(:,3);
% Cycle_No_RES6 = 1:length(RES6);
% Real_RES6 = RES6 + IMF6(:,3);

% B0005
EC1 = Cap_HP_B5(20:29);
C1 = 1:10;

EC2 = Cap_HP_B5(31:47);
C2 = 1:17;

EC3 = Cap_HP_B5(48:89);
C3 = 1:42;

EC4 = Cap_HP_B5(90:119);
C4 = 1:30;

% B0006
EC1_B6 = Cap_HP_B6(20:29);
C1_B6  = 1:10;

EC2_B6 = Cap_HP_B6(31:42);
C2_B6  = 1:12;

EC3_B6 = Cap_HP_B6(48:77);
C3_B6  = 1:30;

EC4_B6 = Cap_HP_B6(90:102);
C4_B6  = 1:13;

% B0007
EC1_B7 = Cap_HP_B7(20:29);
C1_B7 = 1:10;

EC2_B7 = Cap_HP_B7(31:47);
C2_B7 = 1:17;

EC3_B7 = Cap_HP_B7(48:56);
C3_B7 = 1:9;

EC4_B7 = Cap_HP_B7(90:99);
C4_B7 = 1:10;

% B0018
EC1_B18 = Cap_HP_B18(10:22);
C1_B18 = 1:13;

EC2_B18 = Cap_HP_B18(25:39);
C2_B18 = 1:15;

EC3_B18 = Cap_HP_B18(40:45);
C3_B18 = 1:6;

EC4_B18 = Cap_HP_B18(46:55);
C4_B18 = 1:10;

EC5_B18 = Cap_HP_B18(56:70);
C5_B18 = 1:15;

EC6_B18 = Cap_HP_B18(106:120);
C6_B18 = 1:15;

EC7_B18 = Cap_HP_B18(121:132);
C7_B18 = 1:12;

% B00034
EC1_B34 = Cap_HP_B34(22:42);
C1_B34 = 1:21;

EC2_B34 = Cap_HP_B34(46:66);
C2_B34 = 1:21;

EC3_B34 = Cap_HP_B34(67:80);
C3_B34 = 1:14;

% Exponential decline rate data points
% EDR = [0.1085 0.1898 0.0487 0.1944 0.08371 0.1303 0.09318 0.372 0.206 0.2685 0.0843 0.4354 0.0585 0.2444 0.4884 0.1609 0.1518 0.1616 0.1515];
EDR = [0.1085 0.1898 0.1944 0.1303 0.09318 0.206 0.2685 0.0843 0.2444 0.1609 0.1518 0.1616 0.1515];

% Data collection for LSTM
% kt0 = 60; % Prediction start point
j= 10; % # Steps backward, increasing may cause overfitting (K.Liu et al)
m = 5; % # Steps ahead for prediction

% Real Residuals
Cycle_No = 1:length(RES1);
Real_RES1 = RES1 + IMF1(:,3);
Real_RES2 = RES2 + IMF2(:,3);
Real_RES3 = RES3 + IMF3(:,3);
Cycle_No_RES4 = 1:length(RES4);
Real_RES4 = RES4 + IMF4(:,3);
Cycle_No_RES5 = 1:length(RES5);
Real_RES5 = RES5 + IMF5(:,3);
Cycle_No_RES6 = 1:length(RES6);
Real_RES6 = RES6 + IMF6(:,3);

% Real_RES123 = [Real_RES1; Real_RES2; Real_RES3];

XTrain1 = {}; % Empty cell
YTrain1 = {}; % Empty cell

XVal1 = {};
YVal1 = {};

% for k = (j+1):length(Real_RES2)-1
% %     XTrain1{end+1} = Real_RES1(k-j:k);
% %     YTrain1{end+1} = Real_RES1(k+m);
%     XTrain1{end+1} = Real_RES2(k-j:k);
%     YTrain1{end+1} = Real_RES2(k+m);
% end

for k = (j+1):length(RES2)-m
%     XTrain1{end+1} = Real_RES1(k-j:k);
%     YTrain1{end+1} = Real_RES1(k+m);
    XTrain1{end+1} = RES2(k-j:k);
    YTrain1{end+1} = RES2(k+1:k+m)';
end

for k = (j+1):length(RES3)-m
%     XTrain1{end+1} = Real_RES1(k-j:k);
%     YTrain1{end+1} = Real_RES1(k+m);
    XTrain1{end+1} = RES3(k-j:k);
    YTrain1{end+1} = RES3(k+1:k+m)';
end

for k = (j+1):length(RES1)-m
%     XTrain1{end+1} = Real_RES1(k-j:k);
%     YTrain1{end+1} = Real_RES1(k+m);
    XVal1{end+1} = RES1(k-j:k);
    YVal1{end+1} = RES1(k+1:k+m)';
end

% XTest1 = {}; % Empty cell
% YTest1 = {}; % Empty cell
% 
% for k = kt0:length(Real_RES2)-1
%     XTest1{end+1} = Real_RES2(k-j:k);
%     YTest1{end+1} = Real_RES2(k+m);
% end

% Regen_ampl(28:29) = [0.04929 0.0582]
% Regen_ampl(32:33) = [0.07321 0.0717];
% 
% % Regen_Rest_time_fit = [Regen_Rest_time(1:12) Regen_Rest_time(14:20) Regen_Rest_time(22:25) Regen_Rest_time(27) Regen_Rest_time(30:31) Regen_Rest_time(34)];
% % Regen_ampl_fit = [Regen_ampl(1:12) Regen_ampl(14:20) Regen_ampl(22:25) Regen_ampl(27) Regen_ampl(30:31) Regen_ampl(34)];
% % SOH_fit = [SOH(1:12) SOH(14:20) SOH(22:25) SOH(27) SOH(30:31) SOH(34)];
% 
% % Max Capacity cap strategy (Assuming no need for early cycle predictions)
% Regen_cycle_fit = [Regen_cycle(3:8) Regen_cycle(10:12) Regen_cycle(14:17) Regen_cycle(18:20) Regen_cycle(22:25) Regen_cycle(27:29) Regen_cycle(30:34)];
% Regen_Rest_time_fit = [Regen_Rest_time(3:8) Regen_Rest_time(10:12) Regen_Rest_time(14:17) Regen_Rest_time(18:20) Regen_Rest_time(22:25) Regen_Rest_time(27:29) Regen_Rest_time(30:34)];
% Regen_ampl_fit = [Regen_ampl(3:8) Regen_ampl(10:12) Regen_ampl(14:17) Regen_ampl(18:20) Regen_ampl(22:25) Regen_ampl(27:29) Regen_ampl(30:34)];
% SOH_fit = [SOH(3:8) SOH(10:12) SOH(14:17) SOH(18:20) SOH(22:25) SOH(27:29) SOH(30:34)];
% 
% % Capacity decline rate extraction
% C_decl_rate = zeros(1,23);
% for m=2:length(Regen_cycle_fit)
%     C_decl = Capacity_series(Regen_cycle_fit(m)-1) - Capacity_series(Regen_cycle_fit(m-1));
%     C_decl_rate(m) = abs(C_decl / (Regen_cycle_fit(m) - Regen_cycle_fit(m-1)));
% %     if C_decl_rate() > 0
% %         C_decl_rate()
% %         
% %     end
% end
% 
% % Gradient_list_B5_B18 = [-0.001484000000000,-0.002365000000000,-0.001691000000000,-0.002233000000000];
% % Residual_gradient_list = [];
% % Residual_gradient_list(1:168) = abs(Gradient_list_B5_B18(1));
% % Residual_gradient_list(169:336) = abs(Gradient_list_B5_B18(2));
% % Residual_gradient_list(337:504) = abs(Gradient_list_B5_B18(3));
% % Residual_gradient_list(505:636) = abs(Gradient_list_B5_B18(4));
% 
% % Correlating the Residudal gradient
% % Regen_cycle_fit = [Regen_cycle(3:7) Regen_cycle(10:12) Regen_cycle(14)];
% % Regen_Rest_time_fit = [Regen_Rest_time(3:7) Regen_Rest_time(10:12) Regen_Rest_time(14)];
% % Regen_ampl_fit = [Regen_ampl(3:7) Regen_ampl(10:12) Regen_ampl(14)];
% % SOH_fit = [SOH(3:7) SOH(10:12) SOH(14)];
% % Residual_gradient_list_fit = [Residual_gradient_list(3:7) Residual_gradient_list(10:12) Residual_gradient_list(14)];
% % Residual_grad_fit = [Res_grad_fit(3:7) Res_grad_fit(10:12) Res_grad_fit(14)];
% 
% % Individual battery fitting
% % B0006
% SOH_B6 = SOH_fit(3:6);
% Regen_Rest_time_fit_B6 = Regen_Rest_time_fit(3:6);
% Regen_cycle_fit_B6 = Regen_cycle_fit(3:6)-168;
% Regen_ampl_fit_B6 = Regen_ampl_fit(3:6);
% 
% % B0007
% SOH_B7 = SOH_fit(7:9);
% Regen_Rest_time_fit_B7 = Regen_Rest_time_fit(7:9);
% Regen_cycle_fit_B7 = Regen_cycle_fit(7:9)-336;
% Regen_ampl_fit_B7 = Regen_ampl_fit(7:9);
% 
% % B0018
% SOH_B18 = SOH_fit(10:16);
% Regen_Rest_time_fit_B18 = Regen_Rest_time_fit(10:16);
% Regen_cycle_fit_B18 = Regen_cycle_fit(10:16)-504;
% Regen_ampl_fit_B18 = Regen_ampl_fit(10:16);
% 
% % B0054
% SOH_B54 = SOH_fit(18:20);
% Regen_Rest_time_fit_B54 = Regen_Rest_time_fit(18:20);
% Regen_cycle_fit_B54 = Regen_cycle_fit(18:20)-693;
% Regen_ampl_fit_B54 = Regen_ampl_fit(18:20);
% 
% % B0055
% SOH_B55 = SOH_fit(21:24);
% Regen_Rest_time_fit_B55 = Regen_Rest_time_fit(21:24);
% Regen_cycle_fit_B55 = Regen_cycle_fit(21:24)-797;
% Regen_ampl_fit_B55 = Regen_ampl_fit(21:24);
% 
% % B0056
% SOH_B56 = SOH_fit(25:28);
% Regen_Rest_time_fit_B56 = Regen_Rest_time_fit(25:28);
% Regen_cycle_fit_B56 = Regen_cycle_fit(25:28)-899;
% Regen_ampl_fit_B56 = Regen_ampl_fit(25:28);
% 
% % Relating no of cycles degraded to RAmpl for all batteries
% Regen_cycle_fit_shifted = [Regen_cycle_fit_B6 Regen_cycle_fit_B7 Regen_ampl_fit_B18 Regen_ampl_fit_B54];
% Regen_Rest_time_fit_shifted = [Regen_Rest_time_fit(3:6) Regen_Rest_time_fit(7:9) Regen_Rest_time_fit(10:16) Regen_Rest_time_fit(18:20)];
% Regen_ampl_fit_shifted = [Regen_ampl_fit(3:6) Regen_ampl_fit(7:9) Regen_ampl_fit(10:16) Regen_ampl_fit(18:20)];