% Prediction Script
% Signal 2
p00 =     0.05887; 
p10 =   4.041e-07; 
p01 =       0.143;
p11 =  -4.115e-07;
p02 =     -0.1939;
RAmpl = [];
b_sig2 = [];
Sig2 = ones(1,168);
for i = 1:length(Regen_cycle(1:4))
    x = Regen_Rest_time(i);
    y = SOH(i);
    RAmpl(i) = p00 + p10*x + p01*y + p11*x*y + p02*y^2;
%     b_sig2(i) = normrnd(0.191152, 0.023752);    % mu = 0.191152; sigma = 0.123752;
    b_sig2(i) = normrnd(0.165014, 0.0552485);    % mu = 0.191152; sigma = 0.123752;
    if i==length(Regen_cycle(1:4))
        krk = 1:(168-Regen_cycle(i));
        Sig2(Regen_cycle(i):168-1) = RAmpl(i)*exp(-1*b_sig2(i)*krk) + 1;
    else
        krk = 1:(Regen_cycle(i+1)-Regen_cycle(i));
        Sig2(Regen_cycle(i):Regen_cycle(i+1)-1) = RAmpl(i)*exp(-1*b_sig2(i)*krk) + 1;
    end
end

% Signal 3
% LSTM
numFeatures = size(XTrain1{1},1);
numResponses = size(YTrain1{1},1);
numHiddenUnits1 = 10;
numHiddenUnits2 = 50;
numHiddenUnits3 = 200;

layers = [ ...
    sequenceInputLayer(numFeatures)
%     sequenceFoldingLayer('Name','fold1')
    lstmLayer(numHiddenUnits1,'OutputMode','sequence')
%     dropoutLayer(0.25) %change to RELU
    % BatchNorm
%     batchNormalizationLayer
    lstmLayer(numHiddenUnits2,'OutputMode','sequence') 
    % BatchNorm
%     batchNormalizationLayer
 %   dropoutLayer(0.25) %change to RELU  
    lstmLayer(numHiddenUnits3,'OutputMode','sequence') 
    %Batch Norm
%     batchNormalizationLayer
    dropoutLayer(0.25) %change to RELU     
    fullyConnectedLayer(70)
    dropoutLayer(0.25)   % Probability of dropping responses
    fullyConnectedLayer(numResponses)
%     sequenceUnfoldingLayer('Name','unfold1')
    reluLayer
    regressionLayer];

maxEpochs = 6000;
miniBatchSize = 8;

% lgraph = layerGraph(layers);
% lgraph = connectLayers(lgraph,'fold1/miniBatchSize','unfold1/miniBatchSize');
% analyzeNetwork(lgraph)

options = trainingOptions('sgdm', ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'InitialLearnRate',0.0001, ...
    'GradientThreshold',5, ...
    'Shuffle','never', ...
    'ValidationData', {XVal1, YVal1}, ...
    'Plots','training-progress',...
    'Verbose',1);

net = trainNetwork(XTrain1,YTrain1,layers,options); % Train network with battery B0005 data
% net = trainNetwork(XTrain1,YTrain1,lgraph,options); % Train network with battery B0005 data


kt0 = 40; % Prediction start point
XTest1 = {};
YPred1 = {};
j = 10;
% XTemp_mat = Real_RES2(kt0-j:kt0);
% XTemp_mat = Real_RES3(kt0-j:kt0);
XTemp_mat = RES3(kt0-j:kt0);

% XTest1{end+1} = {Real_RES2(kt0-j:kt0)};

% for kt = kt0:length(Real_RES3)
%     XTemp = {};
%     XTemp{end+1} = Real_RES2(k-j:k);
%     XTest1{end+1} = {XTemp_mat};
%     Px = predict(net, XTest1{end}, 'MiniBatchSize', 1);
%     YPred1{end+1} = Px;
%     XTemp_mat = [XTemp_mat(2:end); cell2mat(Px)];
% end

for kt = kt0:length(RES3)
    %XTemp = {};
    %XTemp{end+1} = Real_RES2(k-j:k);
    XTest1{end+1} = {XTemp_mat};
    Px = predict(net, XTest1{end}, 'MiniBatchSize', 1);
    YPred1{end+1} = Px;
    XTemp_mat = [XTemp_mat(2:end); cell2mat(Px)];
end

YPred1_mat = [];
% Converting YPred1 from cell array to matrix form
for i = 1:length(YPred1)
    YPred1_mat(end+1) = cell2mat(YPred1{i});
end

Sig3_pred = [transpose(RES3(1:40)) YPred1_mat];
% Sig3_pred = [transpose(Real_RES3(1:40)) YPred1_mat];
Sig23 = Sig2.*Sig3_pred(1:168);

% Signal 1
% % Process 3rd IMF also
% Fit_sig1 = Capacity_series(336+1:336+kt0)-Sig23(1:kt0);
% % Fit_sig1 = transpose(IMF2(:,3));
% gprMdl1 = fitrgp(transpose(1:kt0), Fit_sig1, 'KernelFunction','squaredexponential');
% % gprMdl1 = fitrgp(transpose(1:kt0), Fit_sig1, 'KernelFunction','rationalquadratic');
% [Sig1,~,Sig1_int] = predict(gprMdl1, transpose((kt0+1):168));
% % sigma = sqrt((1/kt0)*sum((transpose(Cap_HP_B5(1:kt0))-Sig2(1:kt0)).^2)); % Max Likelihood Estimation; Replace with GPR
% % Sig1 = normrnd(0,sigma,1,168-kt0);

Fit_sig1 = IMF1(:,3);
% gprMdl1 = fitrgp(transpose(1:kt0), Fit_sig1, 'KernelFunction','squaredexponential');
gprMdl1 = fitrgp(transpose(1:length(IMF1(:,3))), Fit_sig1, 'KernelFunction','rationalquadratic'); % Good estimator according to K.Liu et al
Fit_sig2 = IMF2(:,3);
gprMdl2 = updateGPRMdl(gprMdl1, transpose(1:length(IMF2(:,3))), Fit_sig2);
Fit_sig3 = IMF3(1:kt0,3);
gprMdl3 = updateGPRMdl(gprMdl2, transpose(1:kt0), Fit_sig3);

[Sig1,~,Sig1_int] = predict(gprMdl3, transpose(1:168));
% sigma = sqrt((1/kt0)*sum((transpose(Cap_HP_B5(1:kt0))-Sig2(1:kt0)).^2)); % Max Likelihood Estimation; Replace with GPR
% Sig1 = normrnd(0,sigma,1,168-kt0);

% Sig1_full = cat(2, transpose(Fit_sig3), transpose(Sig1));
Sig1_full = Sig1;


Sig123 = Sig1_full + Sig23;

% Empirical model with PF

pf = stateEstimatorPF;
pf.StateEstimationMethod = 'mean';
pf.ResamplingMethod = 'systematic';
pf.MeasurementLikelihoodFcn = @pfCapacityMeasurementLikelihood;
% pf.NumStateVariables = 4;
initialize(pf, 10000, [0.8,-0.02,0.1,-0.001], eye(4));

for k = 1:kt0
    [statePred,covPred] = predict(pf);
    [stateCorrected, stateCov] = correct(pf, Real_RES3(k), k, 0.1);
end

stateEst = getStateEstimate(pf);

C_S_Diffs = (Capacity_series(337:504) - Sig23).^2;
RMSE = sqrt(mean(C_S_Diffs)) % Best RMSE, Sept 2, RMSE = 0.0249

%%api to update mdl - Code obtained from link https://www.mathworks.com/matlabcentral/answers/424553-how-to-add-points-to-a-trained-gp
function newmdl = updateGPRMdl(mdl,Xtrain, Ytrain) % a very simple updating api, assumes defaults, will need to be modified for nondefaults
    kernelparams = mdl.KernelInformation.KernelParameters; 
    kernelfunction = mdl.KernelInformation.Name;
    inisigma = mdl.Sigma;
    beta = mdl.Beta; 
    newmdl = fitrgp(Xtrain, Ytrain, 'FitMethod', 'none', 'Sigma', inisigma, 'Beta', beta, 'KernelFunction', kernelfunction, 'KernelParameters', kernelparams); 
end