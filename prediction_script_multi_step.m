% Regression Learner results; Training Data: RES2, RES3; Testing Data:
% RES1, RES4
% TM  -> ? P: ?
% TM1 -> Optimisable Ensemble; P: same as before
% TM2 -> Linear regression  models; P: Works!! Looks good, needs more
% testing, yes, it works in closed loop
% TM3 -> Stepwise liner regression; P: Looks even better, must test a bit
% more

% Prediction Script
% Signal 2
p00 =     0.05887; 
p10 =   4.041e-07; 
p01 =       0.143;
p11 =  -4.115e-07;
p02 =     -0.1939;

% p00 =     -0.1515; 
% p10 =   3.102e-07; 
% p01 =       0.3356;
% p11 =  -1.699e-07;
% p02 =     -0.1318;

RAmpl = [];
b_sig2 = [];
Sig2 = ones(1,168);
for i = 1:length(Regen_cycle(14:20))
    x = Regen_Rest_time(i);
    y = SOH(i);
    RAmpl(i) = p00 + p10*x + p01*y + p11*x*y + p02*y^2;
%     b_sig2(i) = normrnd(0.191152, 0.023752);    % mu = 0.191152; sigma = 0.123752;
    b_sig2(i) = normrnd(0.165014, 0.0552485);    % mu = 0.191152; sigma = 0.123752;
    if i==length(Regen_cycle(1:4))
        krk = 1:(168-Regen_cycle(i));
        Sig2(Regen_cycle(i):168-1) = RAmpl(i)*exp(-1*b_sig2(i)*krk) + 1;
    else
        krk = 1:(Regen_cycle(i+1)-Regen_cycle(i));
        Sig2(Regen_cycle(i):Regen_cycle(i+1)-1) = RAmpl(i)*exp(-1*b_sig2(i)*krk) + 1;
    end
end

% Signal 3
% LSTM
% numFeatures = size(XTrain1{1},1);
% numResponses = size(YTrain1{1},1);
% numHiddenUnits1 = 10;
% numHiddenUnits2 = 50;
% numHiddenUnits3 = 200;
% 
% layers = [ ...
%     sequenceInputLayer(numFeatures)
% %     sequenceFoldingLayer('Name','fold1')
%     lstmLayer(numHiddenUnits1,'OutputMode','sequence')
% %     dropoutLayer(0.25) %change to RELU
%     % BatchNorm
% %     batchNormalizationLayer
%     lstmLayer(numHiddenUnits2,'OutputMode','sequence') 
%     % BatchNorm
% %     batchNormalizationLayer
%  %   dropoutLayer(0.25) %change to RELU  
%     lstmLayer(numHiddenUnits3,'OutputMode','sequence') 
%     %Batch Norm
% %     batchNormalizationLayer
%     dropoutLayer(0.25) %change to RELU     
%     fullyConnectedLayer(70)
%     reluLayer
%     dropoutLayer(0.25)   % Probability of dropping responses
%     fullyConnectedLayer(numResponses)
% %     sequenceUnfoldingLayer('Name','unfold1')
%     reluLayer
%     regressionLayer];
% 
% maxEpochs = 6000;
% miniBatchSize = 16;
% 
% % lgraph = layerGraph(layers);
% % lgraph = connectLayers(lgraph,'fold1/miniBatchSize','unfold1/miniBatchSize');
% % analyzeNetwork(lgraph)
% 
% options = trainingOptions('adam', ...
%     'MaxEpochs',maxEpochs, ...
%     'MiniBatchSize',miniBatchSize, ...
%     'InitialLearnRate',0.001, ...
%     'GradientThreshold',5, ...
%     'Shuffle','every-epoch', ...
%     'ValidationData', {XVal1, YVal1}, ...
%     'Plots','training-progress',...
%     'Verbose',1);
% 
% % net = trainNetwork(XTrain1,YTrain1,layers,options); % Train network with battery B0005 data
% % net = trainNetwork(XTrain1,YTrain1,lgraph,options); % Train network with battery B0005 data

% Simple LSTM
kt0 = 40; % Prediction start point
m = 5; % # of steps to look ahead
XTest1 = {};
YPred1 = {};
j = 15;

numFeatures = 1;
numHiddenUnits = j+1;
numResponses = 5;

maxEpochs = 200;
miniBatchSize = 8;

XVal_LSTM = {};
YVal_LSTM = {};
for i = 1:length(YVal1)
    XVal_LSTM{end+1} = transpose(XVal1{i});
    YVal_LSTM{end+1} = YVal1{i}';
end

YVal_LSTM_mat = [];
for i = 1:length(YVal1)
    YVal_LSTM_mat = [YVal_LSTM_mat; YVal1{i}];
end

layers = [ ...
    sequenceInputLayer(numFeatures)
    lstmLayer(numHiddenUnits,'OutputMode','last')
    fullyConnectedLayer(numResponses)
    regressionLayer];

options = trainingOptions('adam', ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'InitialLearnRate',0.001, ...
    'GradientThreshold',5, ...
    'Shuffle','every-epoch', ...
    'ValidationData', {XVal_LSTM', YVal_LSTM_mat}, ...
    'Plots','training-progress',...
    'Verbose',1);


XTemp_mat = Real_RES1(kt0-j:kt0);

XTrain1_mat = [];
YTrain1_mat = [];
for i = 1:length(YTrain1)
    XTrain1_mat = [XTrain1_mat; transpose(XTrain1{i})];
    YTrain1_mat = [YTrain1_mat; YTrain1{i}];
end

XTest1_mat = [];
for i = 1:length(XTest1)
    XTest1_mat = [XTest1_mat; transpose(XTest1{i})];
end

% Simple LSTM contd
XTrain_LSTM = {};
YTrain_LSTM = {};
for i = 1:length(YTrain1)
    XTrain_LSTM{end+1} = transpose(XTrain1{i});
    YTrain_LSTM{end+1} = YTrain1{i}';
end

YTrain_LSTM_mat = YTrain1_mat;

XTest_LSTM = {};
% for i = 1:length(XTest1)
%     XTest_LSTM{end+1} = transpose(XTest1{i});
% end

net = trainNetwork(XTrain_LSTM',YTrain_LSTM_mat,layers,options); % Train network with battery B0005 data

% Uncomment here

for kt = kt0:m:length(Real_RES1)
    XTest_LSTM{end+1} = XTemp_mat';
    Px = predict(net, XTest_LSTM{end}, 'MiniBatchSize', 1);
    YPred1{end+1} = Px;
    XTemp_mat = [XTemp_mat((m+1):end); Px'];
end

% Uncomment here
YPred1_mat = [];
% Converting YPred1 from cell array to matrix form
for i = 1:length(YPred1)
    YPred1_mat = [YPred1_mat, YPred1{i}];
end

Sig3_pred_R1 = [transpose(Real_RES1(1:kt0)) YPred1_mat];
figure; plot(Sig3_pred_R1)
hold on
plot(Real_RES1)

XTemp_mat_2 = Real_RES4(kt0-j:kt0);
XTest_LSTM_2 = {};
YPred1 = {};

for kt = kt0:m:length(Real_RES4)
    XTest_LSTM_2{end+1} = XTemp_mat_2';
    Px = predict(net, XTest_LSTM_2{end}, 'MiniBatchSize', 1);
    YPred1{end+1} = Px;
    XTemp_mat_2 = [XTemp_mat_2((m+1):end); Px'];
end

% Uncomment here



YPred1_mat = [];
% Converting YPred1 from cell array to matrix form
for i = 1:length(YPred1)
    YPred1_mat = [YPred1_mat, YPred1{i}];
end

Sig3_pred_R4 = [transpose(Real_RES4(1:kt0)) YPred1_mat];
figure; plot(Sig3_pred_R4)
hold on
plot(Real_RES4)




% XTest_mat_Res2 = [];
% for i = 1:length(XTest_Res2)
%     XTest_mat_Res2 = [XTest_mat_Res2; transpose(XTest_Res2{i})];
% end

% XTest1{end+1} = {Real_RES2(kt0-j:kt0)};

% for kt = kt0:length(Real_RES3)
%     XTemp = {};
%     XTemp{end+1} = Real_RES2(k-j:k);
%     XTest1{end+1} = {XTemp_mat};
%     Px = predict(net, XTest1{end}, 'MiniBatchSize', 1);
%     YPred1{end+1} = Px;
%     XTemp_mat = [XTemp_mat(2:end); cell2mat(Px)];
% end

% Uncomment here

% for kt = kt0:length(RES3)
%     XTest1{end+1} = XTemp_mat;
%     Px = predict(net, XTest1{end}, 'MiniBatchSize', 1);
%     YPred1{end+1} = Px;
%     XTemp_mat = [XTemp_mat(2:end); Px];
% end

% Uncomment here
% XTemp_mat = Real_RES4(kt0-j:kt0);
% 
% YPred1_mat = [];
% Sig3_pred_R4 = [];
% for kt = kt0:m:length(Real_RES4)
%     XTest1{end+1} = XTemp_mat;
%     Px = myNeuralNetworkFunction(XTemp_mat');
%     YPred1_mat = [YPred1_mat, Px];
%     XTemp_mat = [XTemp_mat((m+1):end); Px'];
% end
% 
% Sig3_pred_R4 = [transpose(Real_RES4(1:kt0)) YPred1_mat];

% figure; plot(Sig3_pred_R4)
% hold on
% t4_str = sprintf('R4 j = %d; m = %d',j,m);
% title(t4_str)
% plot(Real_RES4)
% 
% XTemp_mat = Real_RES1(kt0-j:kt0);

% YPred1_mat = [];
% Sig3_pred_R1 = [];
% for kt = kt0:m:length(Real_RES1)
%     XTest1{end+1} = XTemp_mat;
% %     Px = predict(net, XTest1{end}, 'MiniBatchSize', 1);
%     Px = myNeuralNetworkFunction(XTemp_mat');
%     YPred1_mat = [YPred1_mat, Px];
%     XTemp_mat = [XTemp_mat((m+1):end); Px'];
% end
% 
% Sig3_pred_R1 = [transpose(Real_RES1(1:kt0)) YPred1_mat];
% 
% figure; plot(Sig3_pred_R1)
% hold on
% t1_str = sprintf('R1 j = %d; m = %d', j, m);
% title(t1_str)
% plot(Real_RES1)

% Dec_tree_mdl = fitrtree(XTrain1_mat, YTrain1_mat, 'CrossVal', 'on');
% Dec_tree_mdl = fitrtree(XTrain1_mat, YTrain1_mat,'OptimizeHyperparameters','auto',...
%     'HyperparameterOptimizationOptions',struct('AcquisitionFunctionName',...
%     'expected-improvement-plus') );
% 
% % Yfit = predict(Dec_tree_mdl, XTest1_mat);
% 
% % CLosed Loop Prediction
% for kt = kt0:length(RES1)
%     %XTemp = {};
%     %XTemp{end+1} = Real_RES2(k-j:k);
%     XTest1{end+1} = XTemp_mat;
%     Px = predict(Dec_tree_mdl, transpose(XTest1{end}));
%     YPred1{end+1} = Px;
%     XTemp_mat = [XTemp_mat(2:end); Px];
% end

% YPred1_mat = [];
% % Converting YPred1 from cell array to matrix form
% for i = 1:length(YPred1)
%     YPred1_mat = [YPred1_mat,  YPred1{i}];
% end

% Sig3_pred = [transpose(Real_RES3(1:40)) YPred1_mat];
% Sig23 = Sig2.*Sig3_pred(1:168);

% for kt = kt0:length(RES2)
%     %XTemp = {};
%     %XTemp{end+1} = Real_RES2(k-j:k);
%     XTest_Res2{end+1} = XTemp_mat_RES2;
%     Px = predict(Dec_tree_mdl, transpose(XTest_Res2{end}));
%     YPred_Res2{end+1} = Px;
%     XTemp_mat_RES2 = [XTemp_mat_RES2(2:end); Px];
% end
% 
% YPred_mat_Res2 = [];
% % Converting YPred1 from cell array to matrix form
% for i = 1:length(YPred_Res2)
%     YPred_mat_Res2(end+1) = YPred_Res2{i};
% end
% 
% Sig3_pred_Res2 = [transpose(RES2(1:40)) YPred_mat_Res2];
% Sig3_pred = [transpose(Real_RES3(1:40)) YPred1_mat];
% Sig23 = Sig2.*Sig3_pred(1:168);


% Signal 1

% % Process 3rd IMF also
% Fit_sig1 = Capacity_series(336+1:336+kt0)-Sig23(1:kt0);
% % Fit_sig1 = transpose(IMF2(:,3));
% gprMdl1 = fitrgp(transpose(1:kt0), Fit_sig1, 'KernelFunction','squaredexponential');
% % gprMdl1 = fitrgp(transpose(1:kt0), Fit_sig1, 'KernelFunction','rationalquadratic');
% [Sig1,~,Sig1_int] = predict(gprMdl1, transpose((kt0+1):168));
% % sigma = sqrt((1/kt0)*sum((transpose(Cap_HP_B5(1:kt0))-Sig2(1:kt0)).^2)); % Max Likelihood Estimation; Replace with GPR
% % Sig1 = normrnd(0,sigma,1,168-kt0);

Fit_sig1 = IMF3(:,3);
% gprMdl1 = fitrgp(transpose(1:kt0), Fit_sig1, 'KernelFunction','squaredexponential');
gprMdl1 = fitrgp(transpose(1:length(IMF1(:,3))), Fit_sig1, 'KernelFunction','rationalquadratic'); % Good estimator according to K.Liu et al
Fit_sig2 = IMF2(:,3);
gprMdl2 = updateGPRMdl(gprMdl1, transpose(1:length(IMF2(:,3))), Fit_sig2);
Fit_sig3 = IMF1(1:kt0,3);
gprMdl3 = updateGPRMdl(gprMdl2, transpose(1:kt0), Fit_sig3);

[Sig1,~,Sig1_int] = predict(gprMdl3, transpose(1:168));
% sigma = sqrt((1/kt0)*sum((transpose(Cap_HP_B5(1:kt0))-Sig2(1:kt0)).^2)); % Max Likelihood Estimation; Replace with GPR
% Sig1 = normrnd(0,sigma,1,168-kt0);

% Sig1_full = cat(2, transpose(Fit_sig3), transpose(Sig1));
% Sig1_full = Sig1;
% Sig123 = Sig1_full + Sig23;

% Empirical model with PF

% pf = stateEstimatorPF;
% pf.StateEstimationMethod = 'mean';
% pf.ResamplingMethod = 'systematic';
% pf.MeasurementLikelihoodFcn = @pfCapacityMeasurementLikelihood;
% % pf.NumStateVariables = 4;
% initialize(pf, 10000, [0.8,-0.02,0.1,-0.001], eye(4));
% 
% for k = 1:kt0
%     [statePred,covPred] = predict(pf);
%     [stateCorrected, stateCov] = correct(pf, Real_RES3(k), k, 0.1);
% end
% 
% stateEst = getStateEstimate(pf);

% C_S_Diffs = (Capacity_series(337:504) - Sig23).^2;
% RMSE = sqrt(mean(C_S_Diffs)) % Best RMSE, Sept 2, RMSE = 0.0249

% Implementing Decision tree algorithm
% XTrain1_mat = [];
% YTrain1_mat = [];
% for i = 1:length(YTrain1)
%     XTrain1_mat = [XTrain1_mat; transpose(XTrain1{i})];
%     YTrain1_mat = [YTrain1_mat; YTrain1{i}];
% end
% 
% XTest1_mat = [];
% for i = 1:length(XTest1)
%     XTest1_mat = [XTest1_mat; transpose(XTest1{i})];
% end

% XTemp_RLA_mat = RES1(kt0-j:kt0);
% 
% YPred_RLA_mat = [];
% for kt = kt0:m:length(RES1)
%     %XTemp = {};
%     %XTemp{end+1} = Real_RES2(k-j:k);
%     XTest1{end+1} = XTemp_mat';
% %     Px = trainedModel4_j20.predictFcn(XTemp_RLA_mat');
%     YPred_RLA_mat(end+1) = Px;
%     XTemp_RLA_mat = [XTemp_RLA_mat((m+1):end), Px];
% end

% YPred_RLA_mat = [];
% for i = 1:length(YPred_RLA)
%     YPred_RLA_mat(end+1) = YPred_RLA{i};
% end

% Sig3_pred_RLA = [transpose(RES1(1:kt0)) YPred_RLA_mat];

%%api to update mdl - Code obtained from link https://www.mathworks.com/matlabcentral/answers/424553-how-to-add-points-to-a-trained-gp
function newmdl = updateGPRMdl(mdl,Xtrain, Ytrain) % a very simple updating api, assumes defaults, will need to be modified for nondefaults
    kernelparams = mdl.KernelInformation.KernelParameters; 
    kernelfunction = mdl.KernelInformation.Name;
    inisigma = mdl.Sigma;
    beta = mdl.Beta; 
    newmdl = fitrgp(Xtrain, Ytrain, 'FitMethod', 'none', 'Sigma', inisigma, 'Beta', beta, 'KernelFunction', kernelfunction, 'KernelParameters', kernelparams); 
end