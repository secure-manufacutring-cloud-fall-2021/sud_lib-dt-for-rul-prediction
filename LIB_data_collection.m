% Cycle_Data = [B0053.cycle, B0054.cycle, B0055.cycle, B0056.cycle];
Cycle_Data = [B0005.cycle, B0006.cycle, B0007.cycle, B0018.cycle, B0053.cycle, B0054.cycle, B0055.cycle, B0056.cycle];
d_cycle = 1;
Cycle_times = [];
for idx = 1:length(Cycle_Data)
    if strcmp(Cycle_Data(idx).type, 'discharge')
        Time_Series_discharge  = Cycle_Data(idx).data.Time;
        Temperature = Cycle_Data(idx).data.Temperature_measured;
        Load_voltage_discharge = Cycle_Data(idx).data.Voltage_load;         %Voltage at load resistance
        Terminal_voltage_discharge = Cycle_Data(idx).data.Voltage_measured; %Battery Terminal voltage
%         l = 1;
%         for k = 2:length(Time_Series_discharge)
%             T_diff(l) = Time_Series_discharge(k) - Time_Series_discharge(k-1);
%             V_diff(l) = Load_voltage_discharge(k) - Load_voltage_discharge(k-1);
%             V_Gradient(l) = V_diff(l) / T_diff(l);
%             l = l + 1;
%         end
%        mean_VGrad(d_cycle) = mean(V_Gradient(abs(V_Gradient) <= 0.001));
        % Extract the TIEDVD
        %V_in_range_idx = find(Load_voltage_discharge>= 1.9 & Load_voltage_discharge <= 2.86);
%         V_in_range_idx = find(Load_voltage_discharge>= 2.33 & Load_voltage_discharge <= 2.92);
%         TIEDVD(d_cycle) = Time_Series_discharge(V_in_range_idx(length(V_in_range_idx))) - Time_Series_discharge(V_in_range_idx(1));
%         mean_VGrad_full(d_cycle) = (Load_voltage_discharge(V_in_range_idx(length(V_in_range_idx))) - Load_voltage_discharge(V_in_range_idx(1))) / TIEDVD(d_cycle) ;
%         %Temp_in_range_idx = find(Temperature >= 28 & Temperature <= 40);
%         Temp_in_range_idx = find(abs(Cycle_Data(idx).data.Current_load) >= 1);        
%         TIETD(d_cycle) = Time_Series_discharge(Temp_in_range_idx(length(Temp_in_range_idx))) - Time_Series_discharge(Temp_in_range_idx(1));
%         Temperature_Gradient_full(d_cycle) = (Temperature(Temp_in_range_idx(length(Temp_in_range_idx))) - Temperature(Temp_in_range_idx(1))) / TIETD(d_cycle) ;
        Capacity_series_full(d_cycle) = Cycle_Data(idx).data.Capacity;
        % Get the rest times and put them in an array
        Cycle_times(:,d_cycle) = Cycle_Data(idx).time;
        d_cycle = d_cycle + 1;
    end
end

% Normalisation
% Capacity_series = [Capacity_series_full(1:55) / Capacity_series_full(2) Capacity_series_full(57:158) / Capacity_series_full(58) Capacity_series_full(160:262) / Capacity_series_full(161) Capacity_series_full(263:362) / Capacity_series_full(263)];
% Capacity_series = [Capacity_series_full(57:159) / Capacity_series_full(58) Capacity_series_full(160:262) / Capacity_series_full(161) Capacity_series_full(263:362) / Capacity_series_full(263)];
Capacity_series = [Capacity_series_full(1:168) / Capacity_series_full(1) Capacity_series_full(169:336) / Capacity_series_full(169) Capacity_series_full(337:504) / Capacity_series_full(337) Capacity_series_full(505:636) / Capacity_series_full(505) Capacity_series_full(637:692) / Capacity_series_full(638) Capacity_series_full(693:795) / Capacity_series_full(694) Capacity_series_full(796:898) / Capacity_series_full(797) Capacity_series_full(899:999) / Capacity_series_full(899)];
% T_Cycle_times = transpose(Cycle_times);
T_Cycle_times = transpose(Cycle_times);

%Rest time based approach
Regen_Rest_time = [];
Regen_cycle = [];
for r = 2:length(T_Cycle_times)
    RT = etime(T_Cycle_times(r,:), T_Cycle_times(r-1,:)); 
    if  RT >= 1e+05
        Regen_Rest_time = [Regen_Rest_time RT];
        Regen_cycle(end + 1) = r;
    end
end

Regen_Rest_time = [];
Regen_ampl = [];
SOH = [];
for  k = 1:length(Regen_cycle)
   RT = etime(T_Cycle_times(Regen_cycle(k),:), T_Cycle_times(Regen_cycle(k)-1,:));
   Regen_Rest_time = [Regen_Rest_time RT];
%   C_Diff = abs((IMF_C(Regen_cycle(k)) - IMF_C(Regen_cycle(k)-1)) / IMF_C(Regen_cycle(k)-1));
   C_Diff = abs((Capacity_series(Regen_cycle(k)) - Capacity_series(Regen_cycle(k)-1)) / Capacity_series(Regen_cycle(k)-1));
%   C_Diff = abs((Capacity_series(Regen_cycle(k)) - Capacity_series(Regen_cycle(k)-1)));
   SOH = [SOH Capacity_series(Regen_cycle(k)-1)];
   Regen_ampl = [Regen_ampl C_Diff]; 
end

Regen_Ampl(28:29) = [0.04929 0.0582];
Regen_Ampl(32:33) = [0.07321 0.0717];

% Regen_Rest_time_fit = [Regen_Rest_time(1:12) Regen_Rest_time(14:20) Regen_Rest_time(22:25) Regen_Rest_time(27) Regen_Rest_time(30:31) Regen_Rest_time(34)];
% Regen_ampl_fit = [Regen_ampl(1:12) Regen_ampl(14:20) Regen_ampl(22:25) Regen_ampl(27) Regen_ampl(30:31) Regen_ampl(34)];
% SOH_fit = [SOH(1:12) SOH(14:20) SOH(22:25) SOH(27) SOH(30:31) SOH(34)];

% Max Capacity cap strategy (Assuming no need for early cycle predictions)
Regen_cycle_fit = [Regen_cycle(3:8) Regen_cycle(10:12) Regen_cycle(14:17) Regen_cycle(18:20) Regen_cycle(22:25) Regen_cycle(27:29) Regen_cycle(30:34)];
Regen_Rest_time_fit = [Regen_Rest_time(3:8) Regen_Rest_time(10:12) Regen_Rest_time(14:17) Regen_Rest_time(18:20) Regen_Rest_time(22:25) Regen_Rest_time(27:29) Regen_Rest_time(30:34)];
Regen_ampl_fit = [Regen_ampl(3:8) Regen_ampl(10:12) Regen_ampl(14:17) Regen_ampl(18:20) Regen_ampl(22:25) Regen_ampl(27:29) Regen_ampl(30:34)];
SOH_fit = [SOH(3:8) SOH(10:12) SOH(14:17) SOH(18:20) SOH(22:25) SOH(27:29) SOH(30:34)];

% Capacity decline rate extraction
C_decl_rate = zeros(1,23);
for m=2:length(Regen_cycle_fit)
    C_decl = Capacity_series(Regen_cycle_fit(m)-1) - Capacity_series(Regen_cycle_fit(m-1));
    C_decl_rate(m) = abs(C_decl / (Regen_cycle_fit(m) - Regen_cycle_fit(m-1)));
%     if C_decl_rate() > 0
%         C_decl_rate()
%         
%     end
end

% Gradient_list_B5_B18 = [-0.001484000000000,-0.002365000000000,-0.001691000000000,-0.002233000000000];
% Residual_gradient_list = [];
% Residual_gradient_list(1:168) = abs(Gradient_list_B5_B18(1));
% Residual_gradient_list(169:336) = abs(Gradient_list_B5_B18(2));
% Residual_gradient_list(337:504) = abs(Gradient_list_B5_B18(3));
% Residual_gradient_list(505:636) = abs(Gradient_list_B5_B18(4));

% Correlating the Residudal gradient
% Regen_cycle_fit = [Regen_cycle(3:7) Regen_cycle(10:12) Regen_cycle(14)];
% Regen_Rest_time_fit = [Regen_Rest_time(3:7) Regen_Rest_time(10:12) Regen_Rest_time(14)];
% Regen_ampl_fit = [Regen_ampl(3:7) Regen_ampl(10:12) Regen_ampl(14)];
% SOH_fit = [SOH(3:7) SOH(10:12) SOH(14)];
% Residual_gradient_list_fit = [Residual_gradient_list(3:7) Residual_gradient_list(10:12) Residual_gradient_list(14)];
% Residual_grad_fit = [Res_grad_fit(3:7) Res_grad_fit(10:12) Res_grad_fit(14)];

% Individual battery fitting
% B0006
SOH_B6 = SOH_fit(3:6);
Regen_Rest_time_fit_B6 = Regen_Rest_time_fit(3:6);
Regen_cycle_fit_B6 = Regen_cycle_fit(3:6)-168;
Regen_ampl_fit_B6 = Regen_ampl_fit(3:6);

% B0007
SOH_B7 = SOH_fit(7:9);
Regen_Rest_time_fit_B7 = Regen_Rest_time_fit(7:9);
Regen_cycle_fit_B7 = Regen_cycle_fit(7:9)-336;
Regen_ampl_fit_B7 = Regen_ampl_fit(7:9);

% B0018
SOH_B18 = SOH_fit(10:16);
Regen_Rest_time_fit_B18 = Regen_Rest_time_fit(10:16);
Regen_cycle_fit_B18 = Regen_cycle_fit(10:16)-504;
Regen_ampl_fit_B18 = Regen_ampl_fit(10:16);

% B0054
SOH_B54 = SOH_fit(18:20);
Regen_Rest_time_fit_B54 = Regen_Rest_time_fit(18:20);
Regen_cycle_fit_B54 = Regen_cycle_fit(18:20)-693;
Regen_ampl_fit_B54 = Regen_ampl_fit(18:20);

% B0055
SOH_B55 = SOH_fit(21:24);
Regen_Rest_time_fit_B55 = Regen_Rest_time_fit(21:24);
Regen_cycle_fit_B55 = Regen_cycle_fit(21:24)-797;
Regen_ampl_fit_B55 = Regen_ampl_fit(21:24);

% B0056
SOH_B56 = SOH_fit(25:28);
Regen_Rest_time_fit_B56 = Regen_Rest_time_fit(25:28);
Regen_cycle_fit_B56 = Regen_cycle_fit(25:28)-899;
Regen_ampl_fit_B56 = Regen_ampl_fit(25:28);

% Relating no of cycles degraded to RAmpl for all batteries
Regen_cycle_fit_shifted = [Regen_cycle_fit_B6 Regen_cycle_fit_B7 Regen_ampl_fit_B18 Regen_ampl_fit_B54];
Regen_Rest_time_fit_shifted = [Regen_Rest_time_fit(3:6) Regen_Rest_time_fit(7:9) Regen_Rest_time_fit(10:16) Regen_Rest_time_fit(18:20)];
Regen_ampl_fit_shifted = [Regen_ampl_fit(3:6) Regen_ampl_fit(7:9) Regen_ampl_fit(10:16) Regen_ampl_fit(18:20)];