% Cycle_Data = [B0005.cycle, B0006.cycle, B0007.cycle, B0018.cycle];
% Cycle_Data = [B0053.cycle, B0054.cycle, B0055.cycle, B0056.cycle];
Cycle_Data = B0005.cycle;
d_cycle = 1;
Cycle_times = [];
LV_array = zeros(200,256);
%idx = input('Enter index: ')
for idx = 1:length(Cycle_Data)
    if strcmp(Cycle_Data(idx).type, 'discharge')
        Time_Series_discharge  = Cycle_Data(idx).data.Time;
        Temperature = Cycle_Data(idx).data.Temperature_measured;
        Load_voltage_discharge = Cycle_Data(idx).data.Voltage_load;         %Voltage at load resistance
        Terminal_voltage_discharge = Cycle_Data(idx).data.Voltage_measured; %Battery Terminal voltage
        LV_array(idx,:) = Load_voltage_discharge;
        %         l = 1;
%         for k = 2:length(Time_Series_discharge)
%             T_diff(l) = Time_Series_discharge(k) - Time_Series_discharge(k-1);
%             V_diff(l) = Load_voltage_discharge(k) - Load_voltage_discharge(k-1);
%             V_Gradient(l) = V_diff(l) / T_diff(l);
%             l = l + 1;
%         end
%        mean_VGrad(d_cycle) = mean(V_Gradient(abs(V_Gradient) <= 0.001));
        % Extract the TIEDVD
        %V_in_range_idx = find(Load_voltage_discharge>= 1.9 & Load_voltage_discharge <= 2.86);
        V_in_range_idx = find(Load_voltage_discharge>= 2.33 & Load_voltage_discharge <= 2.92);
        TIEDVD(d_cycle) = Time_Series_discharge(V_in_range_idx(length(V_in_range_idx))) - Time_Series_discharge(V_in_range_idx(1));
        mean_VGrad_full(d_cycle) = (Load_voltage_discharge(V_in_range_idx(length(V_in_range_idx))) - Load_voltage_discharge(V_in_range_idx(1))) / TIEDVD(d_cycle) ;
        %Temp_in_range_idx = find(Temperature >= 28 & Temperature <= 40);
        Temp_in_range_idx = find(abs(Cycle_Data(idx).data.Current_load) >= 1);        
        TIETD(d_cycle) = Time_Series_discharge(Temp_in_range_idx(length(Temp_in_range_idx))) - Time_Series_discharge(Temp_in_range_idx(1));
        Temperature_Gradient_full(d_cycle) = (Temperature(Temp_in_range_idx(length(Temp_in_range_idx))) - Temperature(Temp_in_range_idx(1))) / TIETD(d_cycle) ;
        Capacity_series_full(d_cycle) = Cycle_Data(idx).data.Capacity;
        % Get the rest times and put them in an array
        Cycle_times(:,d_cycle) = Cycle_Data(idx).time;
%         a = 1;
%         b = [1/5 1/5 1/5 1/5];
%         Z = filter(b, a, Cycle_Data(idx).data.Temperature_measured);
%         plot(Cycle_Data(idx).data.Time, Z)
%         hold on
%         plot(Cycle_Data(idx).data.Time, Cycle_Data(idx).data.Current_load)
%         hold on
        d_cycle = d_cycle + 1;
    end
end

% Input Features
mean_VGrad = [mean_VGrad_full(1:168) / mean_VGrad_full(1) mean_VGrad_full(169:336) / mean_VGrad_full(169) mean_VGrad_full(337:504) / mean_VGrad_full(337) mean_VGrad_full(505:636) / mean_VGrad_full(505)];
Temperature_Gradient = [Temperature_Gradient_full(1:168) / Temperature_Gradient_full(1) Temperature_Gradient_full(169:336) / Temperature_Gradient_full(169) Temperature_Gradient_full(337:504) / Temperature_Gradient_full(337) Temperature_Gradient_full(505:636) / Temperature_Gradient_full(505)];
X_com = [transpose(mean_VGrad) transpose(Temperature_Gradient)];
X_F_for_NN = transpose(X_com);

% Output Features
Capacity_series = [Capacity_series_full(1:168) / Capacity_series_full(1) Capacity_series_full(169:336) / Capacity_series_full(169) Capacity_series_full(337:504) / Capacity_series_full(337) Capacity_series_full(505:636) / Capacity_series_full(505)];

%Partial Least Squares Regression
[XL, YL, Xs, Ys, Beta, PCTVar, MSE] = plsregress(X_com, transpose(Capacity_series), 2);
Cap_fit = [ones(size(X_com,1),1) X_com]*Beta;
RMSE = sqrt(mean((Cap_fit - transpose(Capacity_series)).^2));

% Code for emd
[IMF, RES] = emd(Capacity_series(1:168));
% subplot(3,1,1)
% plot(sum(transpose(IMF)));
% subplot(3,1,2)
% plot(RES)
% subplot(3,1,3)
% plot(sum(transpose(IMF)) + transpose(RES))
% subplot(4,1,4)
% plot(Capacity_series(337:504))
% imf12_m = mean(IMF(:,1) + IMF(:,2));
% IMF12_s = 
% subplot(4,1,1)
% plot(transpose(IMF(:,1)))
% % hold on
% subplot(4,1,2)
% plot(transpose(IMF(:,2)))
% % hold on
% subplot(4,1,3)
% plot(transpose(IMF(:,3)))
% % hold on
% subplot(4,1,4)
% plot(IMF(:,1)+IMF(:,2))
% % hold on

% d_2 = 1;
% for i55 = 1:length(Cycle_Data_2)
%     if strcmp(Cycle_Data_2(i55).type, 'discharge')
%         Capacity_series_2(d_2) = Cycle_Data_2(i55).data.Capacity;
%         d_2 = d_2 + 1;
%     end
% end
% plot(Capacity_series(1:168))
% hold on
% plot(Capacity_series(169:336))
% hold on
% plot(Capacity_series(337:504))
% hold on
% plot(Capacity_series(505:636))
% hold on
% legend('B0005','B0006','B0007','B0018')

[IMF1, RES1] = emd(Capacity_series(1:168));
[IMF2, RES2] = emd(Capacity_series(169:336));
[IMF3, RES3] = emd(Capacity_series(337:504));
[IMF4, RES4] = emd(Capacity_series(505:636));

% subplot(4,2,1)
% plot(sum(transpose(IMF1)));
% subplot(4,2,2)
% plot(RES1)
% subplot(4,2,3)
% plot(sum(transpose(IMF2)));
% subplot(4,2,4)
% plot(RES2)
% subplot(4,2,5)
% plot(sum(transpose(IMF3)));
% subplot(4,2,6)
% plot(RES3)
% subplot(4,2,7)
% plot(sum(transpose(IMF4)));
% subplot(4,2,8)
% plot(RES4)

% plot(Capacity_series(1:168))
IMF_C = [sum(transpose(IMF1)) sum(transpose(IMF2)) sum(transpose(IMF3)) sum(transpose(IMF4))];
T_Cycle_times = transpose(Cycle_times);

%Rest time based approach
Regen_Rest_time = [];
Regen_cycle = [];
for r = 2:length(Cycle_times)
    RT = etime(T_Cycle_times(r,:), T_Cycle_times(r-1,:)); 
    if  RT >= 1e+05
        Regen_Rest_time =  [Regen_Rest_time RT];
        Regen_cycle(end + 1) = r;
    end
end

% Tip: Get regeneration cycles by visual inspection
% Regen_cycle1 = [20,31,48,90,120,151]; % Regeneration cycles for B0005-0007 
% Regen_cycle2 = [10,25,40,46,56,71,86,91,106,121]; % Regeneration cycles for B0018
% % Regen_cycle = [46,56,71,86,91,106,121]; % Regeneration cycles for B0018
% % Regen_cycle1 = [48,90,120,151]; % Regeneration cycles for B0005-0007 
% % Regen_cycle2 = [40,46,56,71,86,91,106,121]; % Regeneration cycles for B0018
% 
% Regen_cycle = [Regen_cycle1 Regen_cycle1+168 Regen_cycle1+336]; %Regen_cycle2+504];

% Capacity Regeneration Vs. Time
Regen_Rest_time = [];
Regen_ampl = [];
for  k = 1:length(Regen_cycle)
   RT = etime(T_Cycle_times(Regen_cycle(k),:), T_Cycle_times(Regen_cycle(k)-1,:));
   Regen_Rest_time = [Regen_Rest_time RT];
%    C_Diff = abs((IMF_C(Regen_cycle(k)) - IMF_C(Regen_cycle(k)-1)) / IMF_C(Regen_cycle(k)-1));
   C_Diff = abs((Capacity_series(Regen_cycle(k)) - Capacity_series(Regen_cycle(k)-1)) / Capacity_series(Regen_cycle(k)-1));
   Regen_ampl = [Regen_ampl C_Diff];
end

% for ir = 1:length(Regen_cycle)
%     if Regen_cycle(ir)>=1 && Regen_cycle(ir) <= 168
%         Regen_cycle(ir) -= 168;
%     elseif 
%         
% 
% end
% Rest time Vs. Regeneration Amplitude
% subplot(4,1,1)
% stem(Regen_Rest_time(1:4), Regen_ampl(1:4))
% hold on
% % subplot(4,1,2)
% stem(Regen_Rest_time(5:8), Regen_ampl(5:8))
% hold on
% % subplot(4,1,3)
% stem(Regen_Rest_time(9:12), Regen_ampl(9:12))
% hold on
% % subplot(4,1,4)
% stem(Regen_Rest_time(13:20), Regen_ampl(13:20))
% hold on
%stem(Regen_cycle, Regen_Rest_time)
%Regen_cycle_new = [Regen_cycle(1:11); Regen_cycle(12:22)-168; Regen_cycle(23:33)-336; Regen_cycle(34:44)-504];
Gradient_lists_B5 = [(Capacity_series(1)-Capacity_series(19))/(19) (Capacity_series(20)-Capacity_series(30))/(30-19) (Capacity_series(31)-Capacity_series(47))/(47-30) (Capacity_series(48)-Capacity_series(89))/(89-47)];
RAmpl_list_B5 = Regen_ampl(1:4);
Gradient_lists_B6 = [(Capacity_series(168+1)-Capacity_series(168+19))/(19) (Capacity_series(168+20)-Capacity_series(168+30))/(30-19) (Capacity_series(168+31)-Capacity_series(168+47))/(47-30) (Capacity_series(168+48)-Capacity_series(168+89))/(89-47)];
RAmpl_list_B6 = Regen_ampl(5:8);
Gradient_lists_B7 = [(Capacity_series(336+1)-Capacity_series(336+19))/(19) (Capacity_series(336+20)-Capacity_series(336+30))/(30-19) (Capacity_series(336+31)-Capacity_series(336+47))/(47-30) (Capacity_series(336+48)-Capacity_series(336+89))/(89-47)];
RAmpl_list_B7 = Regen_ampl(9:12);


% a = 1;
% b = [1/6 1/6 1/6 1/6 1/6];
% Z = filter(b, a, Capacity_series(1:168));