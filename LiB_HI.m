Cycle_Data = B0005.cycle;
idx = input('Enter index: ');
Time_Series_discharge       = Cycle_Data(idx).data.Time;
Load_voltage_discharge      = Cycle_Data(idx).data.Voltage_load;
Terminal_voltage_discharge  = Cycle_Data(idx).data.Voltage_measured;
l = 1;
V_Gradient = zeros(1,length(Time_Series_discharge)-1);
for k = 2:length(Time_Series_discharge)
    T_diff(l) = Time_Series_discharge(k) - Time_Series_discharge(k-1);
    V_diff(l) = Load_voltage_discharge(k) - Load_voltage_discharge(k-1);
    V_Gradient(l) = V_diff(l) / T_diff(l);
    l = l + 1;
end
%r = 1:l-1
mean_VGrad = mean(V_Gradient(abs(V_Gradient) <= 0.001))
r = 1:l-1;
%plot(Time_Series_discharge,Load_voltage_discharge)
%hold on
plot(r,V_Gradient)