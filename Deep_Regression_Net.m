p00 =     0.05887; 
p10 =   4.041e-07; 
p01 =       0.143;
p11 =  -4.115e-07;
p02 =     -0.1939;

% p00 =     -0.1515; 
% p10 =   3.102e-07; 
% p01 =       0.3356;
% p11 =  -1.699e-07;
% p02 =     -0.1318;

RAmpl = [];
b_sig2 = [];
Sig2 = ones(1,168);
for i = 1:length(Regen_cycle(1:4))
    x = Regen_Rest_time(i);
    y = SOH(i);
    RAmpl(i) = p00 + p10*x + p01*y + p11*x*y + p02*y^2;
%     b_sig2(i) = normrnd(0.191152, 0.023752);    % mu = 0.191152; sigma = 0.123752;
    b_sig2(i) = normrnd(0.165014, 0.0552485);    % mu = 0.191152; sigma = 0.123752;
    if i==length(Regen_cycle(1:4))
        krk = 1:(168-Regen_cycle(i));
        Sig2(Regen_cycle(i):168-1) = RAmpl(i)*exp(-1*b_sig2(i)*krk) + 1;
    else
        krk = 1:(Regen_cycle(i+1)-Regen_cycle(i));
        Sig2(Regen_cycle(i):Regen_cycle(i+1)-1) = RAmpl(i)*exp(-1*b_sig2(i)*krk) + 1;
    end
end

% Training Data: RES2, RES3; Testing Data: RES1, RES4

kt0 = 40; % Prediction start point
j = 15; % Size of input vector
m = 5; % # of steps to look ahead

Deep_net = fitnet([12],'trainbr');
Deep_net.trainParam.epochs = 500;
Deep_net.trainParam.lr = 0.0001;
% Deep_net.performFcn = 'mae';
Deep_net = train(Deep_net, XTrain1_mat', YTrain1_mat');

XTemp_mat = Real_RES4(kt0-j:kt0);

YPred1_mat = [];
Sig3_pred_R4 = [];
for kt = kt0:m:length(Real_RES4)
%     XTest1{end+1} = XTemp_mat;
    Px = Deep_net(XTemp_mat);
    YPred1_mat = [YPred1_mat; Px];
    XTemp_mat = [XTemp_mat((m+1):end); Px];
end

Sig3_pred_R4 = [transpose(Real_RES4(1:kt0)) YPred1_mat'];

figure; plot(Sig3_pred_R4)
hold on
t4_str = sprintf('R4 j = %d; m = %d',j,m);
title(t4_str)
plot(Real_RES4)

XTemp_mat = Real_RES1(kt0-j:kt0);

YPred1_mat = [];
Sig3_pred_R1 = [];
for kt = kt0:m:length(Real_RES1)
%     XTest1{end+1} = XTemp_mat;
    Px = Deep_net(XTemp_mat);
    YPred1_mat = [YPred1_mat; Px];
    XTemp_mat = [XTemp_mat((m+1):end); Px];
end

Sig3_pred_R1 = [transpose(Real_RES1(1:kt0)) YPred1_mat'];

% figure; plot(Sig3_pred_R1)
% hold on
% t1_str = sprintf('R1 j = %d; m = %d',j,m);
% title(t1_str)
% plot(Real_RES1)

% RMSE Calculation
% B0005 
C_S_Diffs_R1 = (Real_RES1' - Sig3_pred_R1(1:length(Real_RES1))).^2;
RMSE_R1 = sqrt(mean(C_S_Diffs_R1))
 
% B0018
C_S_Diffs_R4 = (Real_RES4' - Sig3_pred_R4(1:length(Real_RES4))).^2;
RMSE_R4 = sqrt(mean(C_S_Diffs_R4))

Cap_fit_R1 = (Sig3_pred_R1(1:168)/Sig3_pred_R1(1)).*Sig2;
figure; plot(Cap_fit_R1)
hold on
C_true = Capacity_series(1:168)/(Capacity_series(1));
calc_CI((C_true  - Cap_fit_R1),95)

plot(Capacity_series(1:168)/(Capacity_series(1)))

% Cap_fit_R4 = (Sig3_pred_R1(505:636)/Sig3_pred_R1(505)).*Sig2;
% figure; plot(Cap_fit_R4)
% hold on
% C_true = Capacity_series(505:636)/(Capacity_series(505));
% calc_CI((C_true  - Cap_fit_R4),95)
% 
% plot(Capacity_series(505:636)/(Capacity_series(505)))


%Cap_fit_R1 = (Sig3_pred_R1(1:168)/Capacity_series(1)).*Sig2;
function CI = calc_CI(x, cb)
    %x = randi(50, 1, 100);                      % Create Data
    SEM = std(x)/sqrt(length(x));               % Standard Error
    ts_a1= (1-(0.01*cb))/2;
    ts_a2 = 1 - ts_a1;
    ts = tinv([ts_a1  ts_a2],length(x)-1);      % T-Score
    CI = mean(x) + ts*SEM;                      % Confidence Intervals
end

