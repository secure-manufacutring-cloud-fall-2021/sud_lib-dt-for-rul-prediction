# Sud_lib-dt-for-rul-prediction

This project aims to implement a hierarchical DT framework to predict the RUL of Lithium ion batteries

Updates (Dec 16, 2021)
The Multi_step_data_collection_b5_b34.m file extracts the data from the corresponding battery datasets. The prediction_script_multi_step.m file creates the data in the format for the Level 2 DT and is a trial and error run of a few algorithms for the Level 2 DT. 
THe Deep_regression_net.m file implements the FF NN  to extrapolate the capacity till EoL.
